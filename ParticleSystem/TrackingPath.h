#pragma once
#include "ParticlePath.h"
class TrackingPath : public ParticlePath
{
	sf::Vector2f *target;
public:

	TrackingPath(sf::Vector2f *target)
	{
		this->target = target;
	}

	virtual void run(float *timeElapsed, sf::Vector2f &pos, float life)
	{
		pos = (*target);
	}
};

