#pragma once
#include "ParticlePath.h"
#include "SFML\Graphics\Transform.hpp"
class OrbitPath : public ParticlePath
{
	float phase, dPhase, radius;
	sf::Vector2f *center;
public:

	OrbitPath(float phase, float dPhase, float radius, sf::Vector2f *center)
	{
		this->phase = phase;
		this->dPhase = dPhase;
		this->radius = radius;
		this->center = center;
	}

	virtual void run(float *timeElapsed, sf::Vector2f &pos, float life)
	{
		phase += dPhase*(*timeElapsed);
		pos = (*center) + radius*sf::Vector2f(cos(phase), sin(phase));
	}
	~OrbitPath()
	{
	}
};

