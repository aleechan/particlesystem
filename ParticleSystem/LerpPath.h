#pragma once
#include "ParticlePath.h"
class LerpPath : public ParticlePath
{
public:
	sf::Vector2f v0, v1;
	float initialLife;
	LerpPath(sf::Vector2f v0, sf::Vector2f v1, float initialLife)
	{
		this->initialLife = initialLife;
		this->v0 = v0;
		this->v1 = v1;
	}
	virtual void run(float *timeElapsed, sf::Vector2f &pos, float life)
	{
		pos = lerp(v1,v0,life/initialLife);
	}
	~LerpPath(){}
};

