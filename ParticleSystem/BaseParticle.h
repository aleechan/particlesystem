#pragma once
#include <SFML\Main.hpp>
#include <SFML\System.hpp>
#include "MyMath.h"

class BaseParticle
{
public:
	bool active;
	sf::Vector2f pos;
	float life;
	BaseParticle(sf::Vector2f pos, float life)
	{
		this->pos = pos;
		this->life = life;
		active = true;
	}

	virtual void move(sf::Vector2f delta)
	{
		pos += delta;
	}
	virtual void run(float *timeElapsed) = 0;
	virtual ~BaseParticle(){}
};

