#pragma once
#include "BaseParticle.h"
#include "EmitterPattern.h"
#include "ParticlePath.h"
class Emitter : public BaseParticle
{
protected:
	ParticlePath *path;
	EmitterPattern *pattern;
public:
	Emitter(sf::Vector2f pos, float life, ParticlePath *path, EmitterPattern *pattern);
	virtual void run(float *timeElapsed);
	virtual ~Emitter();
};

