#pragma once
#include "ParticleSystem.h"

class EmitterPattern
{
protected:
	ParticleSystem *sys;
public:
	EmitterPattern(ParticleSystem *sys);
	virtual void run(float *timeElapsed, BaseParticle *p) = 0;
	ParticleSystem* getSystem();
	~EmitterPattern();
};

