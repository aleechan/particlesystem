#pragma once
#include "ParticlePath.h"
#include "BezierCurve.h"
class BezierCurvePath : public ParticlePath
{
public:
	BezierCurve curve;
	float lifeSpan;
	BezierCurvePath(sf::Vector2f *points, int numpoints, float life)
	{
		this->lifeSpan = life;
		for (int i = 0; i < numpoints; i++)
		{
			curve.addFirst(points[i]);
		}
	}
	virtual void run(float *timeElapsed,sf::Vector2f &pos, float life)
	{
		pos = curve.getPoint(life/lifeSpan);
	}
};

