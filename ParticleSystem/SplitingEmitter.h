#pragma once
#include "Emitter.h"
#include <queue>
class SplitingEmitter : public Emitter
{
	std::queue<Emitter*> emitters;
public:
	SplitingEmitter(sf::Vector2f pos, float life, ParticlePath *path, EmitterPattern *pattern, Emitter** emitters, int emitCount);
	~SplitingEmitter();
};

