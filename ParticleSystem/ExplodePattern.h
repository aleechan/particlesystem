#pragma once
#include "EmitterPattern.h"
#include "BasicPath.h"
class ExplodePattern :public EmitterPattern
{
	float partInterval;
	sf::Color c;
	float size, minLife, maxLife, maxVel, counter;
public:
	ExplodePattern(int partsPerSec, sf::Color c, float minLife, float maxLife, float size, float maxVel, ParticleSystem *sys) :EmitterPattern(sys)
	{
		if (partsPerSec > 0)
		{
			this->partInterval = 1.f / partsPerSec;
			if (partInterval == 0)
			{
				partInterval = 0.00001f;
			}
		}
		else
		{
			this->partInterval = 1;
		}
		this->c = c;
		this->minLife = minLife;
		this->maxLife = maxLife;
		this->size = size;
		this->maxVel = maxVel;
	}
	virtual void run(float *timeElapsed, BaseParticle *p)
	{
		sf::Vector2f vel;
		float angle, a;
		a = (minLife / maxLife);
		a = 1.f - a + frandom(2 * a);
		counter += (*timeElapsed);
		if (counter > p->life)
		{
			counter = p->life;
		}
		float counter0 = counter;
		while (counter > partInterval)
		{
			angle = frandom(2 * PI);
			vel.x = maxVel * cos(angle) * a;
			vel.y = maxVel * sin(angle) * a;
			vel = lerp(0.1f*vel, vel, counter / counter0);
			sys->addParticle(Particle(p->pos, minLife + frandom(maxLife - minLife), size, c, vel, sf::Vector2f(0, 0)));
			counter -= partInterval;
		}
	}
};

