#include "Particle.h"

Particle::Particle(sf::Vector2f pos, float life, float size, sf::Color c, sf::Vector2f vel, sf::Vector2f acc) :BaseParticle(pos, life)
{
	this->size = size;
	this->colour = c;
	this->vel = vel;
	this->acc = acc;
}

void Particle::run(float * timeElapsed)
{
	life -= (*timeElapsed);
	vel += acc*(*timeElapsed);
	pos += vel*(*timeElapsed);
	if (life < 0)
	{
		active = false;
	}
}

void Particle::move(sf::Vector2f delta)
{
	vel += delta;
}

float Particle::getSize()
{
	return this->size;
}

sf::Vector2f Particle::getPos()
{
	return pos;
}

sf::Color Particle::getColour()
{
	return colour;
}

Particle::~Particle()
{
}
