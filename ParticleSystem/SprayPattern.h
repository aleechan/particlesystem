#pragma once
#include "EmitterPattern.h"
#include "BasicPath.h"

class SprayPattern : public EmitterPattern
{
	float partInterval;
	sf::Color c;
	float lifeSpan, size, counter;
	sf::Vector2f pos0;
public:
	SprayPattern(float partsPerSec, sf::Color c, float lifeSpan, float size, ParticleSystem *sys) : EmitterPattern(sys)
	{
		this->c = c;
		this->lifeSpan = lifeSpan;
		this->size = size;
		if (partsPerSec > 0)
		{
			this->partInterval = 1 / partsPerSec;
		}
		else
		{
			this->partInterval = 1;
		}
		this->counter = 0;
	}
	virtual void run(float *timeElapsed, BaseParticle *p)
	{
		
		sf::Vector2f pv = p->pos - pos0;
		pv = -normalize(&pv);
		counter += (*timeElapsed);
		if (counter > p->life)
		{
			counter = p->life;
		}
		float counter0 = counter;
		while (counter > partInterval)
		{
			sys->addParticle(Particle(lerp(p->pos,pos0,counter/counter0), lifeSpan*(0.5f + frandom(0.5f)), size, c, (100.f)*pv + sf::Vector2f(sf::Vector2f(-pv.y, pv.x)*(15.f - frandom(30.f))), sf::Vector2f(0, 0)));
			counter -= partInterval;
		}
		pos0 = p->pos;
	}
};

