#include <SFML/Graphics.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Main.hpp>
#include <SFML\Network.hpp>
#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <iostream>
#include <sstream>

#include "ParticleSystemHeaders.h"

sf::Vector2f WinSize(1280, 680);

ParticlePath* randomPath()
{
	float a = frandom(2 * PI);
	return new BasicPath((10 + frandom(200))*sf::Vector2f(cos(a), sin(a)), sf::Vector2f(0, 0));
}

EmitterPattern* randomPattern(float life, ParticleSystem *sys)
{
	switch (irandom(5))
	{
	case 0:
		return new SprayPattern(60, sf::Color(irandom(255), irandom(255), irandom(255)), 0.5 + frandom(1.5f), 2.f + frandom(3), sys);
		break;
	case 1:
		//return new ExplodePattern(15 * frandom(5), sf::Color(irandom(255), irandom(255), irandom(255)), 0.1, 1.0, 1.f + frandom(3), 200.f, sys);
		return new HelixSprayPattern(45, sf::Color(irandom(255), irandom(255), irandom(255)), 1.f, 1.f + frandom(3), frandom(2 * PI), frandom(PI), 20 + frandom(60), 1 + irandom(3), sys);
		break;
	case 2:
		//return new ImplodePattern(15 * frandom(5), sf::Color(irandom(255), irandom(255), irandom(255)), 50.f + frandom(250), life, sys);
		return new SprayPattern(60, sf::Color(irandom(255), irandom(255), irandom(255)), 0.5 + frandom(1.5f), 2.f + frandom(3), sys);
		break;
	case 3:
		return new HelixPattern(45, sf::Color(irandom(255), irandom(255), irandom(255)), 1.f, 1.f + frandom(3), frandom(2 * PI), frandom(PI), 20 + frandom(60), sys);
		break;
	case 4:
		return new HelixSprayPattern(45, sf::Color(irandom(255), irandom(255), irandom(255)), 1.f, 1.f + frandom(3), frandom(2 * PI), frandom(PI), 20 + frandom(60), 1 + irandom(3), sys);
		break;
	default:
		int a = 1 + irandom(3);
		EmitterPattern ** patterns = new EmitterPattern*[a];
		for (int i = 0; i < a; i++)
		{
			patterns[i] = randomPattern(life, sys);
		}
		return new MultiPattern(patterns, a, sys);
	}
}

Emitter* randomEmitter(int level, ParticleSystem *sys)
{
	int patternCount = 1 + irandom(3);
	float life = 1.5f + frandom(3.5f);
	bool splitting = false;
	ParticlePath *path = randomPath();
	EmitterPattern *pattern = randomPattern(life,sys);
	if (irandom(10) < 5)
	{
		splitting = true;
	}
	if (level <= 1)
	{
		splitting = false;
	}
	if (splitting)
	{
		int emitCount = 1 + irandom(level / 2);
		Emitter** emits = new Emitter*[emitCount];
		for (int i = 0; i < emitCount; i++)
		{
			emits[i] = randomEmitter(level - 1, sys);
		}
		return new SplitingEmitter(sf::Vector2f(WinSize / 2.f), life, path, pattern, emits, emitCount);
	}
	else
	{
		pattern = randomPattern(life,sys);
		return new Emitter(sf::Vector2f(WinSize / 2.f), life, path, pattern);
	}
};

int main()
{
	sf::RenderWindow window(sf::VideoMode(WinSize.x,WinSize.y), "");
	//window.setFramerateLimit(60);

	sf::Clock timer;
	sf::Time frameTime, counter;

	float timeElapsed;
	ParticleSystem partSys;

	sf::Vector2f points[30];//for defining bezier curves;
	sf::Vector2f center(0, WinSize.y / 2);

	bool lMouse = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	sf::Vector2f mousePos;

	/*
	partSys.addEmitter(new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 10.f, 
									new BasicPath(sf::Vector2f(100, 0), sf::Vector2f(0, 0)), 
									new SprayPattern(100, sf::Color::Red, 1.5f, 5, &partSys))
					  );
	
	partSys.addEmitter(new Emitter(WinSize/2.f,10.f,
								  new NoPath(),
								  new ExplodePattern(5000,sf::Color::Green,1.0f,1.5f,2,200.f,&partSys))
		              );
	partSys.addEmitter(new Emitter(WinSize / 2.f, 10.f,
								   new NoPath(),
								   new ImplodePattern(500,sf::Color::Blue,300,10.f,&partSys))
				      );
	partSys.addEmitter(new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 10.f,
								   new BasicPath(sf::Vector2f(100, 0), sf::Vector2f(0, 0)),
								   new HelixPattern(150,sf::Color::Magenta,2.f,3,0.f,2*PI,50,&partSys))
	
	partSys.addEmitter(new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 10.f,
								   new BasicPath(sf::Vector2f(100, 0), sf::Vector2f(0, 0)),
								   new HelixSprayPattern(350,sf::Color::Magenta,1.f,3,0.f,2*PI,30,5,&partSys))
					   );
	
	
	for (int i = 0; i < 30; i++)
	{
		points[i] = sf::Vector2f(40 + 40 * i, 300 + 400 * sin(rand()));
	}
	partSys.addEmitter(new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 10.f,
									new BezierCurvePath(points, 30,10.f),
									new HelixSprayPattern(60, sf::Color::Magenta, 1.f, 3, 0.f, 2 * PI, 30, 5, &partSys))
  					   );
	//the rainbow helix emitter
	for (int i = 0; i < 30; i++)
	{
		points[i] = sf::Vector2f(40 + 40 * i, 300 + 400 * sin(rand()));
	}

	partSys.addEmitter(new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 10.f,
								   new BezierCurvePath(points, 30, 10.f),
								   new MultiPattern(new EmitterPattern*[8]
												   {new HelixPattern(75, sf::Color::Red        , 2.f, 3, 1.f, 4.5 * PI, 10, &partSys), 
													new HelixPattern(75, sf::Color::Yellow     , 2.f, 3, 2.f, 4.0 * PI, 15, &partSys),
													new HelixPattern(75, sf::Color(255,170,  0), 2.f, 3, 3.f, 3.5 * PI, 20, &partSys),
													new HelixPattern(75, sf::Color::Green      , 2.f, 3, 4.f, 3.0 * PI, 25, &partSys),
													new HelixPattern(75, sf::Color::Blue       , 2.f, 3, 5.f, 2.5 * PI, 30, &partSys),
													new HelixPattern(75, sf::Color(  0,255,255), 2.f, 3, 6.f, 2.0 * PI, 35, &partSys),
													new HelixPattern(75, sf::Color::Blue       , 2.f, 3, 7.f, 1.5 * PI, 40, &partSys),
													new HelixPattern(75, sf::Color(255,  0,255), 2.f, 3, 8.f, 1.0 * PI, 45, &partSys),
												   },8,&partSys)
					   ));	
	

	partSys.addEmitter(new Emitter(sf::Vector2f(0, 0), 25.f, new OrbitPath(0.f, 1.5*PI, 150.f,&center), new SprayPattern(60, sf::Color::White, 1.f, 4.f, &partSys)));
	partSys.addEmitter(new Emitter(sf::Vector2f(0, 0), 25.f, new OrbitPath(0.f, 1.5*PI, 150.f, &center), new HelixSprayPattern(60, sf::Color::Magenta, 1.f, 3, 0.f, 2 * PI, 30, 5, &partSys)));
	partSys.addEmitter(new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 10.f,
		new QuadBezierPath(new BezierCurveQuad(sf::Vector2f(0, 0), sf::Vector2f(WinSize.x / 2, WinSize.y), sf::Vector2f(WinSize.x, 0)), 10),
		new SprayPattern(60, sf::Color::White, 1.f, 4.f, &partSys)));

	partSys.addEmitter(new ChainEmitter(new Emitter*[3]
										{ new Emitter(sf::Vector2f(0, 0), 5.f, 
														new OrbitPath(0.f, 1.5*PI, 150.f,&center), 
														new SprayPattern(60, sf::Color::White, 1.f, 4.f, &partSys)),
										 new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 4.f,
														new BasicPath(sf::Vector2f(100, 0), sf::Vector2f(0, 0)),
														new HelixSprayPattern(60,sf::Color::Magenta,1.f,3,0.f,2 * PI,30,5,&partSys)),
										 new Emitter(WinSize / 2.f,3.f,
														new NoPath(),
														new ExplodePattern(100,sf::Color::Green,1.0f,1.5f,2,200.f,&partSys)) }, 3)
					   );
	if (true)
	{
		Emitter** test = new Emitter*[3];
		test[0] = new Emitter(sf::Vector2f(0, 0), 5.f, new OrbitPath(0.f, 1.5*PI, 150.f, &center), new SprayPattern(60, sf::Color::White, 1.f, 4.f, &partSys));
		test[1] = new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 4.f, new BasicPath(sf::Vector2f(100, 0), sf::Vector2f(0, 0)), new HelixSprayPattern(60, sf::Color::Magenta, 1.f, 3, 0.f, 2 * PI, 30, 5, &partSys));
		test[2] = new Emitter(WinSize / 2.f, 3.f, new NoPath(), new ExplodePattern(100, sf::Color::Green, 1.0f, 1.5f, 2, 200.f, &partSys));
		partSys.addEmitter(new ChainEmitter(test, 3));
	}
	*/

	if (true)
	{
		Emitter** temp = new Emitter*[3];
		temp[0] = new Emitter(sf::Vector2f(0, 0), 1.5f, new BasicPath(100.f * sf::Vector2f(cos(0.66*PI), sin(0.66*PI)), sf::Vector2f(0, 0)), new SprayPattern(60, sf::Color::Red, 1.f, 3.f, &partSys));
		temp[1] = new Emitter(sf::Vector2f(0, 0), 1.5f, new BasicPath(100.f * sf::Vector2f(cos(1.5*PI), sin(1.5*PI)), sf::Vector2f(0, 0)), new SprayPattern(60, sf::Color::Green, 1.f, 3.f, &partSys));
		temp[2] = new Emitter(sf::Vector2f(0, 0), 1.5f, new BasicPath(100.f * sf::Vector2f(cos(2.5*PI), sin(2.5*PI)), sf::Vector2f(0, 0)), new SprayPattern(60, sf::Color::Blue, 1.f, 3.f, &partSys));
		partSys.addEmitter(new SplitingEmitter(sf::Vector2f(0, WinSize.y / 2), 2.f, new BasicPath(sf::Vector2f(100, 0), sf::Vector2f(0, 0)), new SprayPattern(100, sf::Color::White, 1.5f, 5, &partSys),temp,3));
	}

	//partSys.addTurbulence(new Turbulence(sf::Vector2f(0, 0), 100.f, new NoPath(), new BasicWind(300.f, sf::Vector2f(0.707f, 0.707f))));
	//partSys.addTurbulence(new Turbulence(sf::Vector2f(WinSize.x / 2, WinSize.y / 2), 1000.f, new NoPath(), new BasicImplosion(500)));
	//partSys.addTurbulence(new Turbulence(sf::Vector2f(WinSize.x / 2, WinSize.y / 2), 1000.f, new NoPath(), new BasicRepulsion(5000)));
	//partSys.addTurbulence(new Turbulence(sf::Vector2f(0, 0), 1000.f, new TrackingPath(&mousePos), new RadialImplsion(5000.f, 400.f)));
	for (int i = 0; i < 30; i++)
	{
		points[i] = sf::Vector2f(40 + 40 * i, 300 + 400 * sin(rand()));
	}

	

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		frameTime = timer.restart();
		counter += frameTime;
		mousePos = sf::Vector2f(sf::Mouse::getPosition(window));

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !lMouse)//on mouse pressed
		{
			//partSys.addEmitter(new Emitter(sf::Vector2f(0, 0), 5.f, new TrackingLerpPath(sf::Vector2f(0, 0), &mousePos, 5.f), new SprayPattern(30, sf::Color::White, 1.5, 5, &partSys)));
		}
		lMouse = sf::Mouse::isButtonPressed(sf::Mouse::Left);

		if (counter.asSeconds() >0.3f)//run every n seconds
		{
			for (int i = 0; i < 30; i++)
			{
				points[i] = sf::Vector2f(40 + 40 * i, 340 + 600 * sin(rand()));
			}
			float life = 10 * (0.5f + frandom(0.5f));
			//partSys.addEmitter(randomEmitter(10, &partSys));
			partSys.addEmitter(new Emitter(sf::Vector2f(0, WinSize.y / 2.f), 10.f,
											new BezierCurvePath(points, 30, 10.f),
											new MultiPattern(new EmitterPattern*[3]{
															new HelixSprayPattern(30, sf::Color(irandom(255), irandom(255), irandom(255)), 1.f, 5, 0.f, 2 * PI, 30, 3, &partSys),
															new HelixPattern(30, sf::Color(irandom(255), irandom(255), irandom(255)), 1.f, 3, 0.f, 1.0 * PI, 25, &partSys),
															new SprayPattern(30,sf::Color(irandom(255), irandom(255), irandom(255)),1.f,5,&partSys)},3,&partSys)));
			counter = sf::Time::Zero;
		}
		window.clear();
		
		timeElapsed = frameTime.asSeconds();
		partSys.run(&timeElapsed);
		partSys.render(window);

		window.display();
		//center = sf::Vector2f(sf::Mouse::getPosition(window));
		center += timeElapsed *sf::Vector2f(50, 0);
		//Sleep(10 + frandom(100)); // enable to simulate unstable framerate

		std::stringstream out;
		out << "Particle System Test, FPS: " << (int)(1.f / frameTime.asSeconds()) << " ";
		partSys.getStats(out);
		window.setTitle(out.str());
	}

	return 0;
}