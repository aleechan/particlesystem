#pragma once
#include "TurbulencePattern.h"
class BasicWind :
	public TurbulencePattern
{
	float delta;
	sf::Vector2f dir;
public:

	BasicWind(float delta, sf::Vector2f dir)
	{
		this->delta = delta;
		this->dir = dir;
	}
	virtual void run(float *timeElapsed, std::vector<Particle*> *particles, BaseParticle *p)
	{
		for (int i = 0; i < particles->size(); i++)
		{
			if (particles->at(i) != nullptr)
			{
				particles->at(i)->move(delta*dir*(*timeElapsed));
			}
		}
	}
	~BasicWind()
	{
	}
};

