#pragma once
#include "ParticlePath.h"
class BasicPath : public ParticlePath
{
public:
	sf::Vector2f vel, acc;
	BasicPath(sf::Vector2f vel, sf::Vector2f acc)
	{
		this->vel = vel;
		this->acc = acc;
	}
	virtual void run(float *timeElapsed, sf::Vector2f &pos, float life)
	{
		vel += acc*(*timeElapsed);
		pos += vel*(*timeElapsed);
	}
	virtual void move(sf::Vector2f delta, sf::Vector2f &pos)
	{
		vel += delta;
	}
	~BasicPath(){}
};

