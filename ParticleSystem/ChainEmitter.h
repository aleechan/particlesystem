#pragma once
#include "BaseParticle.h"
#include "Emitter.h"
#include <queue>

class ChainEmitter : public BaseParticle
{
	std::queue<Emitter*> emitters;
	float timeElapsed1;
public:

	ChainEmitter(Emitter **emitters, int emitCount);
	virtual void run(float *timeElapsed);
	virtual ~ChainEmitter();
};

