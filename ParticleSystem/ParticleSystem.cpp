#include "ParticleSystem.h"



int ParticleSystem::getInactiveParticle()
{
	int index = -1;
	if (!emptyParts.empty())
	{
		index = *emptyParts.begin();
		emptyParts.erase(emptyParts.begin());
	}
	return index;
}

int ParticleSystem::getInactiveEmitter()
{
	int index = -1;
	if (!emptyEmits.empty())
	{
		index = emptyEmits.front();
		emptyEmits.pop();
	}
	if (index < -1)
	{
		index = -1;
	}
	return index;
}

int ParticleSystem::getInactiveTurbulence()
{
	int index = -1;
	if (!emptyTurbulence.empty())
	{
		index = emptyTurbulence.front();
		emptyTurbulence.pop();
	}
	if (index < -1)
	{
		index = -1;
	}
	return index;
}

ParticleSystem::ParticleSystem()
{
	active = true;
	calculating = false;
	drawing = false;
	rendered = false;
	thread = std::thread(&ParticleSystem::threadFunction, this);
	verts.setPrimitiveType(sf::Triangles);
}


ParticleSystem::~ParticleSystem()
{
	active = false;
	CV.notify_all();
	thread.join();
	for (int i = 0; i < emitters.size(); i++)
	{
		delete emitters[i];
	}
	particles.clear();
	emitters.clear();
}

void ParticleSystem::getStats(std::stringstream & out)
{
	out << "Particles: " << activeParts << "/" << particles.size();
	out << " Emitters: " << activeEmits << "/" << emitters.size();
	out << " Emitter Time " << EmitTime.asMilliseconds();
	out << " Particle Time " << ParticleTime.asMilliseconds();
	out << " Render Time " << RenderTime.asMilliseconds();
	out << " CleanUp Time " << CleanupTime.asMilliseconds();
	out << " Sleep Time" << SleepTime.asMilliseconds();
}

BaseParticle * ParticleSystem::addEmitter(BaseParticle * item)
{
	if (calculating)
	{
		newEmits.push(item);
	}
	else
	{
		int index = getInactiveEmitter();
		if (index != -1)
		{
			emitters[index] = item;
		}
		else
		{
			emitters.push_back(item);
		}
	}
	return item;
}

void ParticleSystem::addParticle(Particle item)
{
	int index = getInactiveParticle();
	if (index != -1)
	{
		particles[index] = item;
	}
	else
	{
		particles.push_back(item);
	}
}

void ParticleSystem::addTurbulence(Turbulence * item)
{
	//item->setParticles(&this->particles);
	int index = getInactiveTurbulence();
	if (index != -1)
	{
		turbulences[index] = item;
	}
	else
	{
		turbulences.push_back(item);
	}
}

void ParticleSystem::run(float *timeElapsed)
{
	calculating = true;
	this->timeElapsed = timeElapsed;
	activeParts = 0;
	activeEmits = 0;
	CV.notify_all();
}

void ParticleSystem::threadFunction()
{
	std::unique_lock<std::mutex> lock(mtx);
	int index;
	while (active)
	{
		if (calculating)
		{
			SleepTime = timer.restart();
			for (int i = 0; i < emitters.size(); i++)
			{
				if (emitters[i] != nullptr)
				{
					if (emitters[i]->active)
					{
						emitters[i]->run(timeElapsed);
						activeEmits++;
					}
					else
					{
						deadEmits.push(i);
					}
				}
			}
			EmitTime = timer.restart();
			for (int i = 0; i < particles.size(); i++)
			{
				if (particles[i].active)
				{
					particles[i].run(this->timeElapsed);
					activeParts++;
				}
				else
				{
					emptyParts.insert(i);
				}
			}
			ParticleTime = timer.restart();
			for (int i = 0; i < turbulences.size(); i++)
			{
				if (turbulences[i] != nullptr)
				{
					if (turbulences[i]->active)
					{
						turbulences[i]->run(this->timeElapsed);
					}
					else
					{
						deadTurbulence.push(i);
					}
				}
			}

			drawing = true;
			//verts.clear();
			verts.resize(activeParts * 4 );
			int counter = 0;
			float size;
			sf::Vector2f pos;
			sf::Color col;
			for (int i = 0; i < particles.size(); i++)
			{
				if (particles[i].active)
				{
					size = particles[i].getSize();
					pos = particles[i].getPos();
					col = particles[i].getColour();
					verts[counter * 3    ].color = col;
					verts[counter * 3 + 1].color = col;
					verts[counter * 3 + 2].color = col;

					verts[counter * 3    ].position = pos + size*sf::Vector2f(0, -1);
					verts[counter * 3 + 1].position = pos + size*sf::Vector2f(-0.707, 0.707);
					verts[counter * 3 + 2].position = pos + size*sf::Vector2f(0.707, 0.707);

					counter++;
				}
			}
			
			RenderTime = timer.restart();
			rendered = true;
			drawing = false;
			if (waitingForRender)
			{
				renderLock.notify_all();
			}
			verts.resize(counter * 3);
			calculating = false;
		}
		while (!newEmits.empty())
		{
			index = getInactiveEmitter();
			if (index != -1)
			{
				emitters[index] = newEmits.front();
			}
			else
			{
				emitters.push_back(newEmits.front());
			}
			newEmits.pop();
		}
		while (!deadEmits.empty())
		{
			index = deadEmits.front();
			deadEmits.pop();
			delete emitters[index];
			emptyEmits.push(index);
			emitters[index] = nullptr;
		}
		CleanupTime = timer.restart();
		if (!active)
		{
			return;
		}
		if (!calculating)
		{
			CV.wait(lock);
		}
	}
}

void ParticleSystem::render(sf::RenderTexture & tex)
{
	std::unique_lock<std::mutex> lock(mtx);
	waitingForRender = true;
	if (drawing || !rendered)
	{
		renderLock.wait(lock);
	}

	rendered = false;
	tex.draw(verts);
	waitingForRender = false;
}

void ParticleSystem::render(sf::RenderWindow & tex)
{
	std::unique_lock<std::mutex> lock(mtx);
	waitingForRender = true;
	if (drawing || !rendered)
	{
		renderLock.wait(lock);
	}

	rendered = false;
	tex.draw(verts);
	waitingForRender = false;
}