#pragma once
#include "TurbulencePattern.h"
#include "ParticlePath.h"
#include "Particle.h"
#include <vector>
class Turbulence :public BaseParticle
{
protected:
	ParticlePath *path;
	TurbulencePattern *pattern;
	std::vector<Particle*> *particles;
public:
	Turbulence(sf::Vector2f pos, float life, ParticlePath *path, TurbulencePattern *pattern);
	void setParticles(std::vector<Particle*> *particles);
	virtual void run(float *timeElapsed);
	virtual ~Turbulence();
};

