#pragma once
#include "ParticlePath.h"
#include "BezierCurve.h"
class QuadBezierPath : public ParticlePath
{
	BezierCurveQuad *curve;
	float lifeSpan;
public:

	QuadBezierPath(BezierCurveQuad *curve, float lifeSpan)
	{
		this->curve = curve;
		this->lifeSpan = lifeSpan;
	}

	virtual void run(float *timeElapsed, sf::Vector2f &pos, float life)
	{
		pos = curve->getPoint(1- (life / lifeSpan));
	}
	~QuadBezierPath()
	{
		delete curve;
	}
};

