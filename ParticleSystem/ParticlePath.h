#pragma once
#include "BaseParticle.h"

class ParticlePath
{
public:
	virtual void run(float *timeElapsed, sf::Vector2f &pos, float life);
	virtual void move(sf::Vector2f delta, sf::Vector2f &pos);
	
};

