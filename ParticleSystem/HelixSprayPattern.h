#pragma once
#include "EmitterPattern.h"
class HelixSprayPattern : public EmitterPattern
{
	int layers;
	sf::Color c;
	float lifeSpan, size, counter, partInterval;
	float phase, dPhase, radius;
	sf::Vector2f pv, pos0, velNormal;
public:

	HelixSprayPattern(float partsPerSec, sf::Color c, float lifeSpan, float size, float phase, float dPhase, float radius, int layers, ParticleSystem *sys) : EmitterPattern(sys)
	{
		this->c = c;
		this->lifeSpan = lifeSpan;
		this->size = size;
		this->phase = phase;
		this->dPhase = dPhase;
		this->radius = radius;
		this->layers = layers;
		if (partsPerSec > 0)
		{
			this->partInterval = 1 / partsPerSec;
		}
		else
		{
			this->partInterval = 1;
		}
		this->counter = 0;
	}
	virtual void run(float *timeElapsed, BaseParticle *p)
	{
		float dPhase1 = (*timeElapsed)*dPhase;
		pv = p->pos - pos0;
		pv = normalize(&pv);
		velNormal = sf::Vector2f(pv.y, -pv.x);
		counter += (*timeElapsed);
		if (counter > p->life)
		{
			counter = p->life;
		}
		float counter0 = counter;
		while (counter > partInterval)
		{
			for (int i = 0; i < layers; i++)
			{
				sys->addParticle(Particle(lerp(pos0, p->pos, counter / counter0), lifeSpan - counter, size, c, -pv*radius*(i + 1.f) + radius*sin(phase + dPhase1*(counter / counter0))*velNormal*(i + 1.f), sf::Vector2f(0, 0)));
				sys->addParticle(Particle(lerp(pos0, p->pos, counter / counter0), lifeSpan - counter, size, c, -pv*radius*(i + 1.f) - radius*sin(phase + dPhase1*(counter / counter0))*velNormal*(i + 1.f), sf::Vector2f(0, 0)));
			}
			//sys->addParticle(new Particle(lerp(pos0, p->pos, counter / counter0) , lifeSpan - counter, size, c, new BasicPath(-pv*50.f + radius*sin(phase + dPhase1*(counter / counter0))*velNormal, sf::Vector2f(0, 0))));
			//sys->addParticle(new Particle(lerp(pos0, p->pos, counter / counter0) , lifeSpan - counter, size, c, new BasicPath(-pv*50.f - radius*sin(phase + dPhase1*(counter / counter0))*velNormal, sf::Vector2f(0, 0))));
			counter -= partInterval;
		}
		phase += dPhase1;
		pos0 = p->pos;
	}
};

