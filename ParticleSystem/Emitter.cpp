#include "Emitter.h"

Emitter::Emitter(sf::Vector2f pos, float life, ParticlePath * path, EmitterPattern * pattern) :BaseParticle(pos, life)
{
	this->path = path;
	this->pattern = pattern;
}

void Emitter::run(float * timeElapsed)
{
	path->run(timeElapsed, pos, life);
	pattern->run(timeElapsed, this);
	life -= (*timeElapsed);
	if (life < 0)
	{
		active = false;
	}
}

Emitter::~Emitter()
{
	delete path;
	delete pattern;
}
