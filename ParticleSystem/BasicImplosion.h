#pragma once
#include "TurbulencePattern.h"
#include "MyMath.h"
class BasicImplosion :
	public TurbulencePattern
{
	float force;
public:

	BasicImplosion(float force)
	{
		this->force = force;
	}
	virtual void run(float *timeElapsed, std::vector<Particle*>* particles, BaseParticle *p)
	{
		sf::Vector2f dir;
		for (int i = 0; i < particles->size(); i++)
		{
			if (particles->at(i) != nullptr)
			{
				dir = ((p->pos) - (particles->at(i)->pos));
				particles->at(i)->move(force*normalize(dir)/point_distance((p->pos),(particles->at(i)->pos)*(*timeElapsed)));
			}
		}
	}
	~BasicImplosion()
	{
	}
};

