#pragma once
#include <SFML\Graphics.hpp>
#include "BaseParticle.h"
#include "ParticlePath.h"
class Particle : public BaseParticle
{
	float size;
	sf::Color colour;
	//ParticlePath path;
	sf::Vector2f vel, acc;
public:

	Particle(sf::Vector2f pos, float life, float size, sf::Color c,sf::Vector2f vel, sf::Vector2f acc);
	virtual void run(float *timeElapsed);
	virtual void move(sf::Vector2f delta);
	float getSize();
	sf::Vector2f getPos();
	sf::Color getColour();
	virtual ~Particle();
};

