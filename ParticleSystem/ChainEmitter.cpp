#include "ChainEmitter.h"



ChainEmitter::ChainEmitter(Emitter **emitters, int emitCount) : BaseParticle(sf::Vector2f(0, 0), 0)
{
	this->life = 0;
	for (int i = 0; i < emitCount; i++)
	{
		if (&emitters[i] != nullptr)
		{
			this->emitters.push(emitters[i]);
			this->life += (emitters[i]->life);
		}
	}
	delete[] emitters;
}

void ChainEmitter::run(float * timeElapsed)
{
	life -= (*timeElapsed);
	if (!emitters.empty())
	{
		timeElapsed1 = (*timeElapsed);
		if (timeElapsed1 < emitters.front()->life)
		{
			emitters.front()->run(timeElapsed);
		}
		else
		{
			timeElapsed1 -= emitters.front()->life;
			emitters.front()->run(timeElapsed);
			this->pos = emitters.front()->pos;
			delete emitters.front();
			emitters.pop();
			if (!emitters.empty())
			{
				emitters.front()->pos = this->pos;
				emitters.front()->run(&timeElapsed1);
			}
		}
	}
	else
	{
		active = false;
	}
}

ChainEmitter::~ChainEmitter()
{
	if (!emitters.empty())
	{
		delete emitters.front();
		emitters.pop();
	}
}
