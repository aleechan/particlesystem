#include "EmitterStorage.h"



EmitterStorage::EmitterStorage()
{
}

Emitter * EmitterStorage::addEmitter(Emitter * emit)
{
	emitters.push_back(emit);
	return emit;
}

void EmitterStorage::clean()
{
	std::list<Emitter*>::iterator iter = emitters.begin();
	while(iter != emitters.end())
	{
		if (!(*iter)->active)
		{
			emitters.erase(iter++);
		}
		else
		{
			iter++;
		}
	}
}

EmitterStorage::~EmitterStorage()
{
	//emitter should be deleted elsewhere
}
