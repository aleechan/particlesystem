#pragma once
#include "TurbulencePattern.h"
class RadialImplsion : public TurbulencePattern
{
	float radius, force;
public:

	RadialImplsion(float force, float radius)
	{
		this->force = force;
		this->radius = radius;
	}

	virtual void run(float *timeElapsed, std::vector<Particle*>* particles, BaseParticle *p)
	{
		sf::Vector2f dir;
		for (int i = 0; i < particles->size(); i++)
		{
			if (particles->at(i) != nullptr)
			{
				if (sqr_distance((p->pos).x, (p->pos).y, (particles->at(i)->pos).x, (particles->at(i)->pos).y) < radius*radius)
				{
					dir = ((p->pos) - (particles->at(i)->pos));
					particles->at(i)->move(force*normalize(dir) / point_distance((p->pos), (particles->at(i)->pos)*(*timeElapsed)));
				}
			}
		}
	}

	~RadialImplsion()
	{
	}
};

