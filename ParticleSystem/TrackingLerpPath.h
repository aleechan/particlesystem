#pragma once
#include "ParticlePath.h"
class TrackingLerpPath : public ParticlePath
{
	sf::Vector2f v0, *target;
	float initialLife;
public:

	TrackingLerpPath(sf::Vector2f v0, sf::Vector2f *target, float initalLife)
	{
		this->v0 = v0;
		this->target = target;
		this->initialLife = initalLife;
	}
	virtual void run(float *timeElapsed, sf::Vector2f &pos, float life)
	{
		pos = lerp((*target), v0, life / initialLife);
	}

};

