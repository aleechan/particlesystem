#pragma once
#include "BaseParticle.h"
#include "Particle.h"
#include <vector>

class TurbulencePattern
{

public:
	TurbulencePattern();
	virtual void run(float *timeElapsed, std::vector<Particle*>* particles, BaseParticle *p) = 0;
	~TurbulencePattern();
};

