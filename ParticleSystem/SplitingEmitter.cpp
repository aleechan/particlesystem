#include "SplitingEmitter.h"

SplitingEmitter::SplitingEmitter(sf::Vector2f pos, float life, ParticlePath * path, EmitterPattern * pattern, Emitter **emitters, int emitCount) : Emitter(pos, life, path, pattern)
{
	for (int i = 0; i < emitCount; i++)
	{
		if (emitters[i] != nullptr)
		{
			this->emitters.push(emitters[i]);
		}
	}
}

SplitingEmitter::~SplitingEmitter()
{
	ParticleSystem *sys = pattern->getSystem();
	while (!emitters.empty())
	{
		emitters.front()->pos = this->pos;
		sys->addEmitter(emitters.front());
		emitters.pop();
	}
}
