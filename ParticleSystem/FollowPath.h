#pragma once
#include "ParticlePath.h"
class FollowPath : public ParticlePath
{
	sf::Vector2f *target;
	sf::Vector2f delta;
	float velocity;
public:

	FollowPath(sf::Vector2f *target, float velocity)
	{
		this->target = target;
		this->velocity = velocity;
	}

	virtual void run(float *timeElapsed, sf::Vector2f &pos, float life)
	{
		delta = *target - pos;
		delta = normalize(&delta);
		pos += delta*velocity*(*timeElapsed);
	}
};

