#pragma once
#include "BaseParticle.h"
#include "Particle.h"
#include "Turbulence.h"
#include <queue>
#include <thread>
#include <sstream>
#include <SFML\Graphics.hpp>
#include <condition_variable>
#include <mutex>
#include <unordered_set>

class ParticleSystem
{
	sf::Clock timer;
	sf::VertexArray verts;

	std::vector<Particle> particles;
	std::vector<BaseParticle*> emitters;
	std::vector<Turbulence*> turbulences;
	std::unordered_set<int> emptyParts;
	//std::queue<int> emptyParts;
	std::queue<int> emptyEmits, deadEmits;
	std::queue<int> emptyTurbulence, deadTurbulence;
	std::queue<Particle> newParts;
	std::queue<BaseParticle*> newEmits;
	std::queue<Turbulence*> newTurbulences;
	std::thread thread;
	std::mutex mtx;
	std::condition_variable CV, renderLock;

	int activeParts, activeEmits;
	sf::Time EmitTime, ParticleTime, RenderTime, CleanupTime, SleepTime;
	float *timeElapsed;
	bool active, calculating, drawing, rendered, waitingForRender;

	int getInactiveParticle();
	int getInactiveEmitter();
	int getInactiveTurbulence();

public:
	ParticleSystem();
	~ParticleSystem();
	void getStats(std::stringstream &out);
	BaseParticle* addEmitter(BaseParticle *item);
	void addParticle(Particle item);
	void addTurbulence(Turbulence *item);
	void run(float *timeElapsed);
	void threadFunction();
	void render(sf::RenderTexture &tex);
	void render(sf::RenderWindow &tex);
};