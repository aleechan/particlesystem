#pragma once
#include "EmitterPattern.h"
class MultiPattern : public EmitterPattern
{
	EmitterPattern **patterns;
	int patternCount;
public:

	MultiPattern(EmitterPattern **patterns, int numPatterns, ParticleSystem *sys) : EmitterPattern(sys)
	{
		this->patterns = new EmitterPattern*[numPatterns];
		for (int i = 0; i < numPatterns; i++)
		{
			this->patterns[i] = patterns[i];
		}
		this->patternCount = numPatterns;
	}

	virtual void run(float *timeElapsed, BaseParticle *p)
	{
		for (int i = 0; i < patternCount; i++)
		{
			patterns[i]->run(timeElapsed, p);
		}
	}
	~MultiPattern()
	{
		for (int i = 0; i < patternCount; i++)
		{
			delete patterns[i];
		}
		delete[] patterns;
	}
};

