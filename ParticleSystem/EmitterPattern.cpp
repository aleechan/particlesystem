#include "EmitterPattern.h"

EmitterPattern::EmitterPattern(ParticleSystem * sys)
{
	this->sys = sys;
}

ParticleSystem * EmitterPattern::getSystem()
{
	return sys;
}

EmitterPattern::~EmitterPattern()
{
}
