#pragma once
#ifndef _MYMATH
#define _MYMATH

#include <iostream>
#include <windows.h>
#include <SFML\System.hpp>

const double degtorad = 0.0174532925;
const double radtodeg = 57.2957795;
const double PI = 3.141592654;

int inline irandom(int v)
{
	return (int)((v*rand() / RAND_MAX) + 0.5);
}

float inline frandom(float v)
{
	return (v*rand() / RAND_MAX);
}

double inline point_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2 - y1)*(y2 - y1) + (x2 - x1)*(x2 - x1);
	return (sqrt(a));
}

float inline point_distance(sf::Vector2f v1, sf::Vector2f v2)
{
	return sqrt((v2.x - v1.x)*(v2.x - v1.x) + (v2.y - v1.y)*(v2.y - v1.y));
}

double inline sqr_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2 - y1)*(y2 - y1) + (x2 - x1)*(x2 - x1);
	return (a);
}

double inline point_direction(double x1, double y1, double x2, double y2)
{
	double cx = x2 - x1;
	double cy = y2 - y1;
	double a = atan2(cy, cx);
	return a;
}
float inline lerp(float v0, float v1, float t)
{
	return (1 - t)*v0 + t*v1;
}

sf::Vector2f inline lerp(sf::Vector2f v0, sf::Vector2f v1, float t)
{
	return (1 - t)*v0 + t*v1;
}

sf::Vector2f inline normalize(sf::Vector2f *vec)
{
	return (1 / sqrt((vec->x)*vec->x + vec->y*vec->y))*(*vec);
}

sf::Vector2f inline normalize(sf::Vector2f vec)
{
	return (1 / sqrt((vec.x)*vec.x + vec.y*vec.y))*(vec);
}

#endif