#pragma once
#include "EmitterPattern.h"
#include "NoPath.h"

class basicPattern : public EmitterPattern
{
	sf::Color c;
	float lifeSpan, size;
public:
	basicPattern(sf::Color c, float lifeSpan, float size, ParticleSystem *sys) : EmitterPattern(sys)
	{
		this->c = c;
		this->lifeSpan = lifeSpan;
		this->size = size;
	}
	virtual void run(float *timeElapsed, BaseParticle *p)
	{
		sys->addParticle(Particle(p->pos, lifeSpan, size, c, sf::Vector2f(0,0),sf::Vector2f(0,0)));
	}
};