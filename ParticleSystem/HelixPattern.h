#pragma once
#include "EmitterPattern.h"
class HelixPattern : public EmitterPattern
{
	sf::Color c;
	float lifeSpan, size, counter, partInterval;
	float phase, dPhase, radius;
	sf::Vector2f pv, pos0, velNormal;
public:
	HelixPattern(float partsPerSec, sf::Color c, float lifeSpan, float size, float phase, float dPhase, float radius, ParticleSystem *sys) : EmitterPattern(sys)
	{
		this->c = c;
		this->lifeSpan = lifeSpan;
		this->size = size;
		this->phase = phase;
		this->dPhase = dPhase;
		this->radius = radius;
		if (partsPerSec > 0)
		{
			this->partInterval = 1 / partsPerSec;
		}
		else
		{
			this->partInterval = 1;
		}
		this->counter = 0;
	}
	virtual void run(float *timeElapsed, BaseParticle *p)
	{
		float dPhase1 = (*timeElapsed)*dPhase;
		pv = p->pos - pos0;
		pv = normalize(&pv);
		velNormal = sf::Vector2f(pv.y, -pv.x);
		counter += (*timeElapsed);
		if (counter > p->life)
		{
			counter = p->life;
		}
		float counter0 = counter;
		while (counter > partInterval)
		{
			
			sys->addParticle(Particle(lerp(pos0, p->pos, counter / counter0) + radius*sin(phase + dPhase1*(counter / counter0))*velNormal, lifeSpan - counter, size, c, -pv*100.f, sf::Vector2f(0, 0)));
			sys->addParticle(Particle(lerp(pos0, p->pos, counter / counter0) - radius*sin(phase + dPhase1*(counter / counter0))*velNormal, lifeSpan - counter, size, c, -pv*100.f, sf::Vector2f(0, 0)));
			counter -= partInterval;
		}
		phase += dPhase1;
		pos0 = p->pos;
	}
};

