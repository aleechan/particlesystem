#include "Turbulence.h"

Turbulence::Turbulence(sf::Vector2f pos, float life, ParticlePath *path, TurbulencePattern *pattern):BaseParticle(pos,life)
{
	this->path = path;
	this->pattern = pattern;
}

void Turbulence::setParticles(std::vector<Particle*>* particles)
{
	this->particles = particles;
}

void Turbulence::run(float * timeElapsed)
{
	path->run(timeElapsed, pos, life);
	pattern->run(timeElapsed,particles,this);
	life -= *timeElapsed;
	if (life < 0)
	{
		active = false;
	}
}

Turbulence::~Turbulence()
{
}
