#pragma once
#include "Emitter.h"
#include <queue>
#include <list>
class EmitterStorage
{
	std::list<Emitter*> emitters;
public:
	EmitterStorage();
	Emitter* addEmitter(Emitter *emit);
	void clean();
	~EmitterStorage();
};

