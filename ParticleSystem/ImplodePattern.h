#pragma once
#include "EmitterPattern.h"
class ImplodePattern : public EmitterPattern
{
	sf::Vector2f temp; //used for placing particles
	sf::Color c;
	float radius, partInterval, counter,life;
public:
	ImplodePattern(int partsPerSec, sf::Color c, float radius, float life, ParticleSystem *sys) :EmitterPattern(sys)
	{
		if (partsPerSec > 0)
		{
			this->partInterval = 1.f / partsPerSec;
			if (partInterval == 0)
			{
				partInterval = 0.00001f;
			}
		}
		else
		{
			this->partInterval = 1;
		}
		this->c = c;
		this->radius = radius;
		this->counter = 0;
		this->life = life;
	}
	virtual void run(float *timeElapsed, BaseParticle *p)
	{
		counter += (*timeElapsed);
		if (counter > p->life)
		{
			counter = p->life;
		}
		life -= (*timeElapsed);
		float counter0 = counter;
		float dir;
		while (counter > partInterval)
		{
			dir = frandom(PI * 2.f);
			temp.x = sin(dir);
			temp.y = cos(dir);
			temp *= radius;
			sys->addParticle(Particle(p->pos + temp, life - counter, 3.f, c,-temp*(radius/(life-counter)),sf::Vector2f(0,0)));
			//sys->addParticle(new Particle(pos + temp, -temp / 3.f, sf::Vector2f(0, 0), 3.f, col), index);
			counter -= partInterval;
		}
	}
};

